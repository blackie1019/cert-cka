# cert-cka

Preparation arrangement for Certified Kubernetes Administrator(CKA)

[https://www.cncf.io/certification/cka/](https://www.cncf.io/certification/cka/)

- [Exam Curriculum (Topics)](https://github.com/cncf/curriculum)
- [Candidate Handbook](https://www.cncf.io/certification/candidate-handbook)
- [Exam Tips](https://training.linuxfoundation.org/go//Important-Tips-CKA-CKAD)

## Exam Curriculum (v1.23)

### 25% - Cluster Architecture, Installation & Configuration

- Manage role based access control (RBAC)
- Use Kubeadm to install a basic cluster
- Manage a highly-available Kubernetes cluster
- Provision underlying infrastructure to deploy a Kubernetes cluster
- Perform a version upgrade on a Kubernetes cluster using Kubeadm
- Implement etcd backup and restore

### 15% - Workloads & Scheduling

- Understand deployments and how to perform rolling update and rollbacks
- Use ConfigMaps and Secrets to configure applications
- Know how to scale applications
- Understand the primitives used to create robust, self-healing, application deployments
- Understand how resource limits can affect Pod scheduling
- Awareness of manifest management and common templating tools

### 20% - Services & Networking

- Understand host networking configuration on the cluster nodes
- Understand connectivity between Pods
- Understand ClusterIP, NodePort, LoadBalancer service types and endpoints
- Know how to use Ingress controllers and Ingress resources
- Know how to configure and use CoreDNS
- Choose an appropriate container network interface plugin

### 10% - Storage

- Understand storage classes, persistent volumes
- Understand volume mode, access modes and reclaim policies for volumes
- Understand persistent volume claims primitive
- Know how to configure applications with persistent storage

### 30% - Troubleshooting

- Evaluate cluster and node logging
- Understand how to monitor applications
- Manage container stdout & stderr logs
- Troubleshoot application failure
- Troubleshoot cluster component failure
- Troubleshoot networking

## Exam Details

- The exams are delivered online and consist of performance-based tasks (problems) to be solved on the command line running Linux.
- The exams consist of 15-20 performance-based tasks.
- Candidates have 2 hours to complete the CKA and CKAD exam.
- The exams are proctored remotely via streaming audio, video, and screen sharing feeds.
- Results will be emailed 24 hours from the time that the exam is completed.

![1.png](1.png)

following additional command-line tools pre-installed and pre-configured:
- `kubectl` with `k` alias and `Bash` autocompletion
- `jq` for YAML/JSON processing
- `tmux` for terminal multiplexing
- `curl` and `wget` for testing web services
- `man` and man pages for further documentation

## bookmarks

- [https://kubernetes.io/docs/](https://kubernetes.io/docs/)
- [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

## tips

- [Certified Kubernetes Administrator (CKA) with Practice Tests](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/learn/lecture/14224074#overview)
  - [https://github.com/kodekloudhub/certified-kubernetes-administrator-course](https://github.com/kodekloudhub/certified-kubernetes-administrator-course)
- [alijahnas/CKA-practice-exercises: This is a guide for passing the CNCF Certified Kubernetes Administrator (CKA) with practice exercises. Good luck! (github.com)](https://github.com/alijahnas/CKA-practice-exercises)
- [chadmcrowell/CKA-Exercises: Practice for the Certified Kubernetes Administrator (CKA) Exam (github.com)](https://github.com/chadmcrowell/CKA-Exercises)

## Exam & Labs

- [https://killer.sh/](https://killer.sh/)
- [Udemy Labs – Certified Kubernetes Administrator with Practice Tests](https://kodekloud.com/courses/labs-certified-kubernetes-administrator-with-practice-tests/)

## References

- [張景隆 - CKA 考試全攻略流程](https://medium.com/@Appletone/cka-%E8%80%83%E8%A9%A6%E5%85%A8%E6%94%BB%E7%95%A5%E6%B5%81%E7%A8%8B-3a28d1b73eea)
- [https://github.com/mmumshad/kubernetes-the-hard-way](https://github.com/mmumshad/kubernetes-the-hard-way)