# Core Concepts

- Master
- Wortker Nodes

- ETCD Cluster
- kube-scheduler
- Controller-Manager
  - Node-Controller
  - Replication-Controller
- Kube-apiserver
- container runtime engine
- kubelet
- kube-proxy

## ETCD

- key-value store

```sh
# SET value with key
./etcdctl set key1 value1
# Get value with key
./etcdctl get key1 
```

## ETCD Service

`2379` as default port

- setup manual
- setup kubeadm

## kube-apiserver

kube-api server is the only component that interacts directly with etcd datastore

1. Authenticate User
2. Validate Request
3. Retrieve data
4. Update ETCD
5. Scheduler
6. Kubelet

From ADM:

```sh
kubectl get pods -n kube-system
```

From Manual:

```sh
cat /etc/systemd/system/kube-apiserver.service
```

From process:

```sh
ps -aux | grep kube-apiserver
```

## Kube-Controller-Manager

--node-monitor-period=5s
--node-monitor-grace-period=40s
--pod-eviction-timeout=5m0s

From ADM:

```sh
kubectl get pods -n kube-system
```

or

```sh
cat /etc/kubernetes/manifests/kube-controller-manager.yml
```

From Manual:

```sh
cat /etc/systemd/system/kube-controller-manager.service
```

From process:

```sh
ps -aux | grep kube-controller-manager
```

## Kube-Scheduler

1. Filter Nodes
2. Rank Nodes

From ADM:

```sh
kubectl get pods -n kube-system
```

or

```sh
cat /etc/kubernetes/manifests/kube-scheduler.yml
```

From Manual:

```sh
cat /etc/systemd/system/kube-scheduler.service
```

From process:

```sh
ps -aux | grep kube-scheduler
```

## Kubelet

1. Register Node
2. Create PODs
3. Monitor Node & PODs

*Only Manually Install(not include in Kubeadm)*

From process:

```sh
ps -aux | grep kubelet
```

## Kube-proxy

POD Network

From ADM:

```sh
kubectl get pods -n kube-system
kubectl get daemonset -n kube-system
```

```sh
kubectl run nginx --image nginx
kubectl get pods
```

## POD with YAML

```yml
apiVersion: v1
kind: Pod
metadata:
  name: myapp-rc
  labels:
    app: myapp
    type: front-end
spec:
  container:
    - name: my-nginx
      image: nginx
```

![2.png](../2.png)

```sh
kubectl create -f {xxx.yml}
kubectl get pods
kubectl describe pod {myapp-pod}
```

>The key difference between kubectl `apply` and `create` is that apply creates Kubernetes objects through a declarative syntax, while the create command is imperative. The command set kubectl apply is used at a terminal's command-line window to create or modify Kubernetes resources defined in a manifest file

kubectl create v.s. kubectl apply

### kubectl create

- kubectl create是所謂的"命令式管理"(Imperative Management)。通過這種方法，可以告訴Kubernetes API你要創建，替換或刪除的內容。
- kubectl create命令，是先刪除所有現有的東西，重新根據YAML文件生成新的。所以要求YAML文件中的配置必須是完整的
- kubectl create命令，用同一個YAML文件重複創建會失敗。

### kubectl apply

- kubectl apply是"聲明式管理"(Declarative Management)方法的一部分，在該方法中，即使對目標用了其他更改，也可以"保持"你對目標（即按比例縮放）應用的更改。
- kubectl apply命令，根據配置文件裡面列出来的内容，生及現有的。所以YAML文件的内容可以只寫需要升级的欄位
- 簡單理解就是，kubectl apply可以重複創建；kubectl create只能創建一次

Ref [【從題目中學習k8s】-【Day4】K8s中的resource object(一) - Pod、Deployment](https://ithelp.ithome.com.tw/articles/10235286)

## Replication Controller

### Replication Controller: 舊的

```yml
apiVersion: v1
kind: ReplicationController
metadata:
  name: myapp-rc
  labels:
    app: myapp
    type: front-end
spec:
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      container:
        - name: my-nginx
          image: nginx
  replicas: 3
```

```sh
kubectl create -f {xxx.yml}
kubectl get ReplicationController
kubectl get pods
```

### Replication Set: 新的(建議做法)

可以用已建立的 Pod(用 `selector`)

```yml
apiVersion: apps/v1
kind: ReplicationController
metadata:
  name: myapp-replicaset
  labels:
    app: myapp
    type: front-end
spec:
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      container:
        - name: my-nginx
          image: nginx
  replicas: 3
  selector: 
    matchLabels:
      type: front-end
```

```sh
kubectl create -f {xxx.yml}
kubectl get replicaset
kubectl delete replicaset myapp-replicaset
kubectl replace -f xx.yml
```

### Scaling

```sh
# 方法 1
kubectl replace -f xx.yml

# 方法 2
kubectl scale --replicas=6 xx.yml

# 方法 3
kubectl scale --replicas=6 replicaset myapp-replicaset
```